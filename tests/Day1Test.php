<?php

namespace tests2020;

use y2020\Day1 as Day;
use y2020\src\DayInterface;

final class Day1Test extends DayTestBase {

  protected DayInterface $day;

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}