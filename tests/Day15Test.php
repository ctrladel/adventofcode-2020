<?php

namespace tests2020;

use y2020\Day15 as Day;
use y2020\src\DayInterface;

final class Day15Test extends DayTestBase {

  protected function setUp(): void {
    $this->day = new Day();
  }

  protected static function getDay(): DayInterface {
    return new Day();
  }

}