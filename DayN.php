<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class DayN extends Day {

  protected const DAY = N;

  public function __construct() {
    $this->addExample(1, 1, "", "");
    $this->addExample(2, 1, "", "");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $answer = '';
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}

$day = new DayN();
//$day->setInputs($day->getExample(1, 1)->getInput());
$day->getAnswerPart1();
$day->getAnswerPart2();
