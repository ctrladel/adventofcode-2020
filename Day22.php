<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day22 extends Day {

  protected const DAY = 22;

  public function __construct() {
    $this->addExample(1, 1, "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10", "306");
    $this->addExample(2, 1, "Player 1:\n43\n19\n\nPlayer 2:\n2\n29\n14", "105");
    $this->addExample(2, 2, "Player 1:\n9\n2\n6\n3\n1\n\nPlayer 2:\n5\n8\n4\n7\n10", "291");
  }

  public function processInputs(array $inputs): array {
    $player1 = [];
    $player2 = [];
    $p2 = FALSE;

    foreach ($inputs as $input) {
      if ($input === 'Player 2:') {
        $p2 = TRUE;
        continue;
      }
      if ($input === 'Player 1:' || $input === '') {
        continue;
      }

      if ($p2) {
        $player2[] = (int) $input;
      }
      else {
        $player1[] = (int) $input;
      }
    }

    return [$player1, $player2];
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $p1 = $inputs[0];
    $p2 = $inputs[1];
    $winner = FALSE;
    do {
      $p1c = array_shift($p1);
      $p2c = array_shift($p2);

      if ($p1c > $p2c) {
        $p1[] = $p1c;
        $p1[] = $p2c;
      }
      else {
        $p2[] = $p2c;
        $p2[] = $p1c;
      }

      if (empty($p1)) {
        $winner = $p2;
      }

      if (empty($p2)) {
        $winner = $p1;
      }

    } while (!$winner);

    $score = 0;
    foreach (array_reverse($winner) as $mult => $v) {
      $score += ($mult + 1)*$v;
    }

    $answer = $score;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $p1 = $inputs[0];
    $p2 = $inputs[1];
    $game = 0;
    $prevRounds = [];

    $winner = $this->playGame($p1, $p2, $prevRounds, $game);

    if ($winner === 1) {
      $winner = $p1;
    }
    else {
      $winner = $p2;
    }

    $score = 0;
    foreach (array_reverse($winner) as $mult => $v) {
      $score += ($mult + 1)*$v;
    }

    $answer = $score;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  public function playGame(&$p1, &$p2, $previousRounds, &$game) {
    $game++;

    $winner = FALSE;
    do {
      $p1copy = $p1;
      $p2copy = $p2;

      $roundKey = implode('', $p1);
      if (in_array($roundKey, $previousRounds)) {
        $winner = 1;
      }
      else {
        $previousRounds[] = $roundKey;
        $roundWinner = $this->playRound($p1copy, $p2copy, $previousRounds, $game);

        if ($roundWinner === 1) {
          $p1[] = array_shift($p1);
          $p1[] = array_shift($p2);
        }
        else {
          $p2[] = array_shift($p2);
          $p2[] = array_shift($p1);
        }

        if (empty($p1)) {
          $winner = 2;
        }

        if (empty($p2)) {
          $winner = 1;
        }
      }
    } while(!$winner);

    return $winner;
  }

  public function playRound(&$p1, &$p2, &$previousRounds) {

    $c1 = array_shift($p1);
    $c2 = array_shift($p2);

    if (count($p1) >= $c1 && count($p2) >= $c2) {
      $p1copy = array_slice($p1, 0, $c1);
      $p2copy = array_slice($p2, 0, $c2);
      return $this->playGame($p1copy, $p2copy, $previousRounds, $game);
    }
    else {
      if ($c1 > $c2) {
        return 1;
      }
      else {
        return 2;
      }
    }
  }

}
