<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day11 extends Day {

  protected const DAY = 11;

  public function __construct() {
    $this->addExample(1, 1, "L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL", "37");
    $this->addExample(2, 1, "L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL", "26");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $input = str_split($input);
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $state = $inputs;
    $newState = $inputs;
    $count = 0;
    do {
      $changed = FALSE;
      foreach ($state as $rowK => $columns) {
        foreach ($columns as $columnK => $seatState) {
          $newSeatState = $this->changeByAdjacentSeat($state, $rowK, $columnK);

          if ($newSeatState != $seatState) {
            $changed = TRUE;
            $newState[$rowK][$columnK] = $newSeatState;
          }
        }
      }

      if ($changed) {
        $state = $newState;
        $count++;
      }
    } while($changed == TRUE);

    $occupied = 0;
    foreach ($state as $rowK => $columns) {
      foreach ($columns as $columnK => $seatState) {
        if ($seatState =='#') {
          $occupied++;
        }
      }
    }

    $answer = $occupied;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function changeByAdjacentSeat($currentState, $row, $column) {
    if ($currentState[$row][$column] == '.') {
      return $currentState[$row][$column];
    }

    $adj = [
      [-1, -1],
      [-1, 0],
      [-1, 1],
      [0, -1],
      [0, 1],
      [1, -1],
      [1, 0],
      [1, 1],
    ];

    $adjState = [
      '#' => 0,
      '.' => 0,
      'L' => 0,
    ];

    foreach ($adj as $seat) {
      $seatRow = $row + $seat[0];
      $seatColumn = $column + $seat[1];

      if (isset($currentState[$seatRow]) && isset($currentState[$seatRow][$seatColumn])) {
        $adjState[$currentState[$seatRow][$seatColumn]]++;
      }
    }

    if ($currentState[$row][$column] == 'L') {
      if ($adjState['#'] == 0) {
        return '#';
      }
    }

    if ($currentState[$row][$column] == '#') {
      if ($adjState['#'] >= 4) {
        return 'L';
      }
    }

    return $currentState[$row][$column];
  }

  public function changeBySeatsInView($currentState, $row, $column) {
    if ($currentState[$row][$column] == '.') {
      return $currentState[$row][$column];
    }
    $adj = [
      [-1, -1],
      [-1, 0],
      [-1, 1],
      [0, -1],
      [0, 1],
      [1, -1],
      [1, 0],
      [1, 1],
    ];

    $adjPos = [
      [$row + -1, $column + -1],
      [$row + -1, $column + 0],
      [$row + -1, $column + 1],
      [$row + 0, $column + -1],
      [$row + 0, $column + 1],
      [$row + 1, $column + -1],
      [$row + 1, $column + 0],
      [$row + 1, $column + 1],
    ];

    $adjState = [
      '#' => 0,
      '.' => 0,
      'L' => 0,
    ];


    do {
      foreach ($adjPos as $k => $seat) {
        $seatRow = $seat[0];
        $seatColumn = $seat[1];

        if (isset($currentState[$seatRow]) && isset($currentState[$seatRow][$seatColumn])) {
          if ($currentState[$seatRow][$seatColumn] == '#' || $currentState[$seatRow][$seatColumn] == 'L') {
            $adjState[$currentState[$seatRow][$seatColumn]]++;
            unset($adjPos[$k]);
            unset($adj[$k]);
          }
          else {
            $adjPos[$k][0] += $adj[$k][0];
            $adjPos[$k][1] += $adj[$k][1];
          }
        }
        else {
          unset($adjPos[$k]);
          unset($adj[$k]);
        }
      }
    } while ($adjPos);

    if ($currentState[$row][$column] == 'L') {
      if ($adjState['#'] == 0) {
        return '#';
      }
    }

    if ($currentState[$row][$column] == '#') {
      if ($adjState['#'] >= 5) {
        return 'L';
      }
    }

    return $currentState[$row][$column];
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $state = $inputs;
    $newState = $inputs;
    $changed = FALSE;
    $count = 0;
    do {
      $changed = FALSE;
      foreach ($state as $rowK => $columns) {
        foreach ($columns as $columnK => $seat) {
          $newSeatState = $this->changeBySeatsInView($state, $rowK, $columnK);

          if ($newSeatState != $seat) {
            $changed = TRUE;
            $newState[$rowK][$columnK] = $newSeatState;
          }
        }
      }

      if ($changed) {
        $state = $newState;
        $count++;
      }
    } while($changed == TRUE);

    $occupied = 0;
    foreach ($state as $rowK => $columns) {
      foreach ($columns as $columnK => $seat) {
        if ($seat =='#') {
          $occupied++;
        }
      }
    }

    $answer = $occupied;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
