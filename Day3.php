<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day3 extends Day {

  protected const DAY = 3;

  public function __construct() {
    $this->addExample(1, 1, "..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#", "7");
    $this->addExample(2, 1, "..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#", "336");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $input = str_split($input);
    }
    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $answer = $this->traverseSlope($inputs, 0, 0, 3, 1);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $slopes = [
      [1,1],
      [3,1],
      [5,1],
      [7,1],
      [1,2],
    ];

    $answer = [];
    foreach ($slopes as $slope) {
      $answer[] = $this->traverseSlope($inputs, 0, 0, $slope[0], $slope[1]);
    }

    $answer = array_product($answer);
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  public function traverseSlope(array $slope, int $startX, int $startY, int $travX, int $travY, $charToCount = '#') {
    $posX = $startX;
    $posY = $startY;

    $height = count($slope) - 1;
    $width = count($slope[0]);
    $count = 0;
    do {
      $posX += $travX;
      $posY += $travY;

      $absX = $posX % $width;

      if ($slope[$posY][$absX] === $charToCount) {
        $count++;
      }

    } while ($posY < $height);

    return $count;
  }

}

