<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day1 extends Day {

  protected const DAY = 1;

  protected int $target;

  public function __construct() {
    $this->addExample(1, 1, "1721\n979\n366\n299\n675\n1456", "514579");
    $this->addExample(2, 1, "1721\n979\n366\n299\n675\n1456", "241861950");

    $this->target = 2020;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();
    $answers = $this->findTargetSumWithNValues($this->target, 2, $inputs);
    $answer = array_product($answers);
    echo "\n\nPART 1\n";
    echo "Numbers: " . implode(',', $answers) ."\n";
    echo "Answer: " . array_product($answers);

    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();
    $answers = $this->findTargetSumWithNValues($this->target, 3, $inputs);
    $answer = array_product($answers);
    echo "\n\nPART 2\n";
    echo "Numbers: " . implode(',', $answers) ."\n";
    echo "Answer: " . $answer;

    return $answer;
  }

  function findTargetSumWithNValues($target, $numberOfValues, $values) {
    $answers = [];
    foreach ($values as $key => $value) {
      if ($numberOfValues > 2) {
        $available = $values;
        unset($available[$key]);
        $answers = $this->findTargetSumWithNValues($target - $value, $numberOfValues-1, $available);
        if ($answers) {
          $answers[] = $value;
          break;
        }
      }
      else {
        foreach ($values as $value2) {
          if (($value + $value2) === $target) {
            return [$value, $value2];
          }
        }
      }
    }

    return $answers;
  }

}
