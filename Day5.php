<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day5 extends Day {

  protected const DAY = 5;

  public function __construct() {
    $this->addExample(1, 0, "FBFBBFFRLR", "357");
    $this->addExample(1, 1, "BFFFBBFRRR", "567");
    $this->addExample(1, 2, "FFFBBBFRRR", "119");
    $this->addExample(1, 3, "BBFFBBFRLL", "820");
  }

  public function processInputs(array $inputs): array {
    foreach ($inputs as &$input) {
      $input = [
        'row' => substr($input, 0, 7),
        'column' => substr($input, 7),
      ];
    }

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $seats = [];
    foreach ($inputs as $input) {
      $row = $this->processRows($input['row']);
      $column = $this->processColumn($input['column']);
      $seat = $row * 8 + $column;
      $seats[] = $seat;
    }

    $answer = max($seats);
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $seats = [];
    foreach ($inputs as $k => $input) {
      $row = $this->processRows($input['row']);
      $column = $this->processColumn($input['column']);
      $seat = $row * 8 + $column;
      $seats[] = $seat;
    }

    for ($i = 0; $i<=1311; $i++) {
      if (!in_array($i, $seats) && (in_array($i - 1, $seats) && in_array($i + 1, $seats))) {
        $seat = $i;
        break;
      }
    }

    $answer = $seat;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

  public function processRows($row, $totalRows = 127) {
    $input = str_split($row);

    $possibleRows = [
      'start' => 0,
      'end' => $totalRows,
    ];
    foreach ($input as $b) {
      if ($b === 'F') {
        $possibleRows['end'] = $possibleRows['end'] - ceil(($possibleRows['end'] - $possibleRows['start'])/2);
      }
      else {
        $possibleRows['start'] = ceil(($possibleRows['end'] - $possibleRows['start'])/2) + $possibleRows['start'];
      }
    }

    return $possibleRows['start'];
  }

  public function processColumn($column, $totalColumns = 8) {
    $input = str_split($column);

    $possibleColumns = [
      'start' => 0,
      'end' => $totalColumns,
    ];
    foreach ($input as $b) {
      if ($b === 'L') {
        $possibleColumns['end'] = $possibleColumns['end'] - ceil(($possibleColumns['end'] - $possibleColumns['start'])/2);
      }
      else {
        $possibleColumns['start'] = ceil(($possibleColumns['end'] - $possibleColumns['start'])/2) + $possibleColumns['start'];
      }
    }

    return $possibleColumns['start'];
  }

}
