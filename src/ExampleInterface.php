<?php


namespace y2020\src;


interface ExampleInterface {

  public function setInput($input);

  public function getInput();

  public function getAnswer(): string;

  public function getNumber(): string;

  /**
   * @return array
   */
  public function getArgs(): array;

}