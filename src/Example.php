<?php
declare(strict_types=1);

namespace y2020\src;


final class Example implements ExampleInterface {

  protected $input = '';

  protected $answer = '';

  protected $part = '';

  protected $number = '';

  protected $args = [];

  public function __construct($part, $number, $input, string $answer, array $args) {
    $this->input = $input;
    $this->answer = $answer;
    $this->part = (string) $part;
    $this->number = (string) $number;
    $this->args = $args;
  }

  public function setInput($input) {
    $this->input = $input;
  }

  public function getInput() {
    return $this->input;
  }

  public function getAnswer(): string {
    return $this->answer;
  }

  public function getNumber(): string {
    return $this->part . "." . $this->number;
  }

  /**
   * @return array
   */
  public function getArgs(): array {
    return $this->args;
  }

}