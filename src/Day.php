<?php

namespace y2020\src;

use aoc\Utility\FileReader;

abstract class Day implements DayInterface {

  protected const DAY = '';

  protected string $rawInputs = '';

  protected array $inputs = [];

  protected array $examples = [
    1 => [],
    2 => [],
  ];

  /**
   * {@inheritdoc}
   */
  public function getInputs(string $separator = "\n"): array {
    if ($this->rawInputs) {
      $inputs = $this->rawInputs;
    }
    else {
      $inputs = (new FileReader('day' . static::DAY . '.txt', '2020'))->getFileContents();
    }

    $inputs = explode($separator, $inputs);

    return $this->processInputs($inputs);
  }

  /**
   * {@inheritdoc}
   */
  public function getAnswers(string $separator = "\n"): array {

    $answerPath = __DIR__ . '/../../../answers/2020/day' . static::DAY . '.json';
    if (!file_exists($answerPath)) {
      return [];
    }
    $answers = file_get_contents($answerPath);
    $answers = json_decode($answers, TRUE);

    return $answers;
  }

  public function setInputs(string $inputs) {
    $this->rawInputs = $inputs;
  }

  public function processInputs(array $inputs): array {
    return $inputs;
  }

  public function addExample(int $part, int $number, $input, string $answer, array $args = []): void {
    $this->examples[$part][$number] = new Example($part, $number, $input, $answer, $args);
  }

  public function getExample(string $example): Example {
    if (!isset($this->examples[$example])) {
      throw new \Exception('Example not found');
    }

    return $this->examples[$example];
  }

  public function getExamples(): array {
    return $this->examples;
  }


}