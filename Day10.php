<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day10 extends Day {

  protected const DAY = 10;

  public function __construct() {
    $this->addExample(1, 1, "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4", "35");
    $this->addExample(1, 2, "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3", "220");
    $this->addExample(2, 1, "16\n10\n15\n5\n1\n11\n7\n19\n6\n12\n4", "8");
    $this->addExample(2, 2, "28\n33\n18\n42\n31\n14\n46\n20\n48\n47\n24\n23\n49\n45\n19\n38\n39\n11\n1\n32\n25\n35\n8\n17\n7\n9\n4\n2\n34\n10\n3", "19208");
  }

  public function processInputs(array $inputs): array {

    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    $inputs[] = 0;
    sort($inputs);

    $diffs = [
      0 => 0,
      1 => 0,
      2 => 0,
      3 => 0,
    ];
    foreach ($inputs as $k => $input) {
      if ($k === count($inputs) - 1) {
        $diff = 3;
      }
      else {
        $diff = $inputs[$k + 1] - $input;
      }

      $diffs[$diff]++;
    }

    $answer = $diffs[1] * $diffs[3];
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    $inputs[] = 0;
    array_push($inputs, max($inputs) + 3);
    sort($inputs);

    $ways = [0 => 1];
    for ($i = 0; $i < count($inputs); $i++) {
      for ($j = $i + 1; $j < count($inputs); $j++) {
        if (!isset($ways[$j])) {
          $ways[$j] = 0;
        }
        if (($inputs[$j] - $inputs[$i]) > 3) {
          break;
        }

        $ways[$j] += $ways[$i];
      }
    }

    $answer = $ways[count($ways) - 1];
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
