<?php

namespace y2020;

use y2020\src\Day;

require __DIR__ . '/../../autoload.php';

class Day15 extends Day {

  protected const DAY = 15;

  public function __construct() {
    $this->addExample(1, 1, "0,3,6\n", "436");
    $this->addExample(1, 2, "1,3,2\n", "1");
    $this->addExample(1, 3, "2,1,3\n", "10");
    $this->addExample(1, 4, "1,2,3\n", "27");
    $this->addExample(1, 5, "2,3,1\n", "78");
    $this->addExample(1, 6, "3,2,1\n", "438");
    $this->addExample(1, 7, "3,1,2\n", "1836");
    $this->addExample(2, 1, "0,3,6\n", "175594");
    $this->addExample(2, 2, "1,3,2\n", "2578");
    $this->addExample(2, 3, "2,1,3\n", "3544142");
    $this->addExample(2, 4, "1,2,3\n", "261214");
    $this->addExample(2, 5, "2,3,1\n", "6895259");
    $this->addExample(2, 6, "3,2,1\n", "18");
    $this->addExample(2, 7, "3,1,2\n", "362");
  }

  public function processInputs(array $inputs): array {

    $inputs = explode(',', $inputs[0]);
    return $inputs;
  }

  public function getAnswerPart1() {
    $inputs = $this->getInputs();

    foreach ($inputs as $k => $input) {
      $seen[$input] = [
        0 => null,
        1 => $k,
      ];
    }

    $next = $inputs[count($inputs) - 1];
    for ($i = count($inputs); $i < 2020; $i++) {
      $lastSpoken = $next;
      if (isset($seen[$lastSpoken])) {
        if (is_null($seen[$lastSpoken][0])) {
          $next = 0;
        }
        else {
          $next = $seen[$lastSpoken][1] - $seen[$lastSpoken][0];
        }
      }
      else {
        $next = 0;
      }

      $a = 5;

      if (isset($seen[$next])) {
        $seen[$next][0] = $seen[$next][1];
        $seen[$next][1] = $i;
      }
      else {
        $seen[$next] = [
          0 => null,
          1 => $i,
        ];
      }
    }

    $answer = $next;
    echo "\n\nPART 1\n";
    echo "Answer: $answer";
    return $answer;
  }

  public function getAnswerPart2() {
    $inputs = $this->getInputs();

    foreach ($inputs as $k => $input) {
      $seen[$input] = [
        0 => null,
        1 => $k,
      ];
    }

    $next = $inputs[count($inputs) - 1];
    for ($i = count($inputs); $i < 30000000; $i++) {
      $lastSpoken = $next;
      if (isset($seen[$lastSpoken])) {
        if (is_null($seen[$lastSpoken][0])) {
          $next = 0;
        }
        else {
          $next = $seen[$lastSpoken][1] - $seen[$lastSpoken][0];
        }
      }
      else {
        $next = 0;
      }

      if (isset($seen[$next])) {
        $seen[$next][0] = $seen[$next][1];
        $seen[$next][1] = $i;
      }
      else {
        $seen[$next] = [
          0 => null,
          1 => $i,
        ];
      }
    }

    $answer = $next;
    echo "\n\nPART 2\n";
    echo "Answer: $answer" ;
    return $answer;
  }

}
